CC := g++
FLAGS := -g -Wall -std=c++0x
CMP := $(CC) $(FLAGS)

TARGET := bin/cptable

$(TARGET): build/main.o build/text_processor.o build/tableutils.o
	$(CMP) -o $(TARGET) build/main.o build/text_processor.o build/tableutils.o

build/main.o: src/main.cpp src/tableutils.h
	$(CMP) -o build/main.o -c src/main.cpp

build/text_processor.o: src/text_processor.cpp
	$(CMP) -o build/text_processor.o -c src/text_processor.cpp

build/tableutils.o: src/tableutils.cpp src/tableutils.h src/text_processor.h
	$(CMP) -o build/tableutils.o -c src/tableutils.cpp

clean:
	rm -r $(TARGET) build/*
