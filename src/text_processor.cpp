#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

vector<string> split_string(string line, string delim, bool repeat_delim) {
    size_t pos=0, dlen=delim.length();
    
    vector<string> words;
    string word;
    while((pos=line.find(delim)) != string::npos) {
        word = line.substr(0, pos);
        line.erase(0, pos + dlen);

        if(repeat_delim && word.length() == 0) {
            continue;
        } else {
            words.push_back(word);
        }

    }
    
    if(line.length() > 0) {
        words.push_back(line);
    }

    return words;
}

vector<string> split_string(string line, string delim) {
    return split_string(line, delim, false);
}

string get_filetext(string filepath) {
    ifstream inf(filepath);

    ostringstream data;
    if (inf) {
        data << inf.rdbuf();
        inf.close();
        return data.str();
    } else {
        cerr << "Invalid input file." << endl;
        throw;
    }
}
