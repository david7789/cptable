#include <string>
#include <vector>
#include <iostream>

#include "tableutils.h"
#include "text_processor.h"

using namespace std;

void debug(string msg)
{
    if (true) {
        cout << msg << endl;
    }
}

void Table::init(string infilepath, string delim, bool headerline, bool repeat_delim)
{
    vector<string> lines = split_string(get_filetext(infilepath), "\n");
    
    int cols = -1;

    if (headerline) {
        // populate headers
        headers = split_string(lines[0], delim, repeat_delim);
        lines.erase(lines.begin());
        cols = headers.size();
    } else {
        cols = split_string(lines[0], delim, repeat_delim).size();
    }

    // populate data
    vector<string> row;
    for(int i=0; i < lines.size(); i++) {
        row = split_string(lines[i], delim, repeat_delim);
        if (row.size() != cols) {
            cout << "[Table{}] ERROR: mismatched columns.  Row: " << i+1 << endl;
            throw;
        } else {
            data.push_back(row);
        }
    }

    cout << "Header1: " << headers[0] << endl;
    cout << "Data1: " << data[0][0] << endl;
}

Table::Table(string infilepath, string delim, bool headerline, bool repeat_delim)
{
    init(infilepath, delim, headerline, repeat_delim);
}

Table::Table(string infilepath, string delim, bool headerline)
{
    // default: repeat_delim=false
    //Table(infilepath, delim, headerline, false);
    init(infilepath, delim, headerline, false);
}

Table::Table(string infilepath, string delim)
{
    // default: headerline=false
    //Table(infilepath, delim, false);
    init(infilepath, delim, false, false);
}

Table::Table(string infilepath)
{
    // default: delim="\t"
    //Table(infilepath, "\t");
    init(infilepath, "\t", false, false);
}

vector<string> Table::get_headers()
{
    //cout << "[get_headers()] Header1: " << headers[0] << endl;
    return headers;
}

void Table::set_headers(vector<string> colnames)
{
    int datacols=data[0].size(), headercols=colnames.size();
    if (datacols != headercols) {
        cout << "[Table::set_headers()] Incorrect number of header cols.  Header: " << headercols << " Data: " << datacols << endl;
        throw;
    } else {
        headers = colnames;
    }
}

vector<vector<string>> Table::get_data()
{
    return data;
}

int Table::row_count()
{
    return data.size();
}
