#ifndef TABLEUTILS_H
#define TABLEUTILS_H

#include <string>
#include <vector>

class Table
{
    private:
        std::vector<std::string> headers;
        std::vector<std::vector<std::string>> data;
    public:
        Table(std::string infilepath, std::string delim, bool headerline, bool repeat_delim);
        Table(std::string infilepath, std::string delim, bool headerline);
        Table(std::string infilepath, std::string delim);
        Table(std::string infilepath);
        void init(std::string infilepath, std::string delim, bool headerline, bool repeat_delim);
        std::vector<std::string> get_headers();
        void set_headers(std::vector<std::string>);
        std::vector<std::vector<std::string>> get_data();
        int row_count();
};
        

#endif