#ifndef TEXT_PROCESSOR_H
#define TEXT_PROCESSOR_H

#include <string>
#include <vector>

std::vector<std::string> split_string(std::string line, std::string delim, bool repeat_delim);
std::vector<std::string> split_string(std::string line, std::string delim);
std::string get_filetext(std::string filepath);

#endif
