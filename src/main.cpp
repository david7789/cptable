#include <string>
#include <vector>
#include <iostream>

#include "tableutils.h"
#include "text_processor.h"

using namespace std;

int main(int argc, char* argv[])
{
    if (argc != 2) {
        cout << "Usage: cptable INFILEPATH" << endl;
        return 1;
    } else {
        string infilepath = argv[1];

        cout << "[begin file text:]\n" << get_filetext(infilepath) << endl;
        Table tbl = Table(infilepath, "\t", true);
        cout << "[:end file text]" << endl;

        vector<string> headers = tbl.get_headers();

        cout << "Table headers: (" << headers.size() << ")" << endl;
        for(int i=0; i < headers.size(); i++) {
            cout << "  [" << i+1 << "] " << headers[i] << endl;
        }

        cout << "Row count: " << tbl.row_count() << endl;
    }
}
